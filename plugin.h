#ifndef PLUGIN_H
#define PLUGIN_H

#include <QObject>
#include <QSslCertificate>
#include <QVariantMap>

class QSslSocket;

class Plugin : public QObject
{
  Q_OBJECT
public:
  Plugin(QObject *parent = 0) : QObject(parent) {}
  virtual ~Plugin() {}

signals:
  void sendToDevice(const QSslCertificate &clientCert, const QString &receiver, const QVariantMap &data);

public slots:
  virtual void receivedFromDevice(const QSslCertificate &clientCert, const QVariantMap &data, QSslSocket *socket = nullptr) = 0;
};

#endif // PLUGIN_H
