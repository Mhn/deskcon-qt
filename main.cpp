//#include "daemon.h"
#include "superconductor.h"
#include "qtdesktopintegration.h"
#include "pluginmanager.h"

#include <QApplication>
#include <QSettings>
#include <QSslCertificate>
#include <QFile>
#include <QUuid>
#include <QHostInfo>
#include <QCommandLineParser>
#include <QCommandLineOption>
#include <QDebug>

#ifdef Q_OS_WIN32
#include <QtCrypto/QtCrypto>
#else
#include <QtCrypto>
#endif

void generateSslCertificates(QString certificatePath, QString privateKeyPath)
{
  QCA::init();
  QCA::PrivateKey privKey = QCA::KeyGenerator().createRSA(2048);
  QCA::CertificateOptions opts;
  opts.setSerialNumber(QString("0x" + QUuid::createUuid().toByteArray()).remove(QRegExp(R"([-\{\}])")).left(15).toLongLong(0, 16));
  opts.setValidityPeriod(QDateTime::currentDateTime(), QDateTime::currentDateTime().addYears(3));
  QCA::CertificateInfo info;
  info.insert(QCA::CommonName, QHostInfo::localHostName());
  opts.setInfo(info);
  QCA::Certificate cert(opts, privKey);

  if (!cert.toPEMFile(certificatePath))
    qWarning() << "Could not create certificate file" << certificatePath;
  if (!privKey.toPEMFile(privateKeyPath))
    qWarning() << "Could not create private key file" << privateKeyPath;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setQuitOnLastWindowClosed(false);

    QCoreApplication::setOrganizationName("apa");
    QCoreApplication::setApplicationName("kaka");
    QCoreApplication::setApplicationVersion("1.0");
    QSettings::setDefaultFormat(QSettings::IniFormat);

    QCommandLineParser parser;

    parser.setApplicationDescription("ApaKaka");
    QCommandLineOption helpOption = parser.addHelpOption();
    QCommandLineOption versionOption = parser.addVersionOption();

    QCommandLineOption portOption(
          QStringList() << "p" << "port",
          QCoreApplication::translate("main", "Set custom port to listen for connections on."),
          QCoreApplication::translate("main", "listening port"));
    parser.addOption(portOption);

    QCommandLineOption certificatePathOption(
          QStringList() << "c" << "certificate",
          QCoreApplication::translate("main", "Custom path to TLS certificate."),
          QCoreApplication::translate("main", "path"));
    parser.addOption(certificatePathOption);

    QCommandLineOption keyPathOption(
          QStringList() << "k" << "privatekey",
          QCoreApplication::translate("main", "Custom path to TLS private key."),
          QCoreApplication::translate("main", "path"));
    parser.addOption(keyPathOption);

    QCommandLineOption casPathOption(
          QStringList() << "cas",
          QCoreApplication::translate("main", "Custom path to TLS CA certificates."),
          QCoreApplication::translate("main", "path"));
    parser.addOption(casPathOption);

    QCommandLineOption writeOption(
          QStringList() << "w" << "write",
          QCoreApplication::translate("main", "Update config file with specified values."));
    parser.addOption(writeOption);

    parser.process(a);

    if (parser.isSet(helpOption) || parser.isSet(versionOption))
      return 0;

    QSettings settings;

    QString certificatePath(settings.value("cerificate", "server.crt").toString());
    QString privateKeyPath(settings.value("privatekey", "server.key").toString());
    QString casPath(settings.value("ca-certificates", "cas.crt").toString());
    uint port(settings.value("port", 8083).toUInt());
    uint udpPort(settings.value("broadcastport", 5108).toUInt());

    if (parser.isSet(portOption))
      port = parser.value(portOption).toUInt();
//    if (parser.isSet(udpPortOption))
//      udpPort = parser.value(udpPortOption).toUInt();
    if (parser.isSet(certificatePathOption))
      certificatePath = parser.value(certificatePathOption);
    if (parser.isSet(keyPathOption))
      privateKeyPath = parser.value(keyPathOption);
    if (parser.isSet(casPathOption))
      casPath = parser.value(casPathOption);

    if (parser.isSet(writeOption))
    {
      settings.setValue("certificate", certificatePath);
      settings.setValue("privateKey", privateKeyPath);
      settings.setValue("port", port);
      settings.setValue("broadcastport", udpPort);
      settings.setValue("ca-certificates", casPath);
      settings.sync();
    }

    QFile cert_file(certificatePath);
    QFile key_file(privateKeyPath);

    if (!cert_file.exists() || !key_file.exists())
    {
      generateSslCertificates(certificatePath, privateKeyPath);
    }

    QSslCertificate certificate;
    if (cert_file.open(QIODevice::ReadOnly)) {
      certificate = QSslCertificate(&cert_file, QSsl::Pem);
      cert_file.close();
    }
    else
      qWarning() << "Could not read certificate file" << cert_file.errorString();

    QSslKey privateKey;
    if (key_file.open(QIODevice::ReadOnly)) {
      privateKey = QSslKey(&key_file, QSsl::Rsa, QSsl::Pem);
      key_file.close();
    }
    else
      qWarning() << "Could not read private key file" << key_file.errorString();

    qDebug() << certificate.subjectInfo(QSslCertificate::CommonName);


    QStringList list = PluginFactory::instance()->types();
    qDebug() << "Plugins:" << list;

    SuperConductor daemon(certificate, privateKey, casPath, port, udpPort, &a);
//    Daemon daemon2(&settings2);
    QtDesktopIntegration desktopIntegration(certificate, casPath, &a);

    QObject::connect(PluginManager::instance(), &PluginManager::sendToDevice, &daemon, &SuperConductor::sendToDevice);
    QObject::connect(&daemon, &SuperConductor::receivedFromDevice, PluginManager::instance(), &PluginManager::receivedFromDevice);
    QObject::connect(&daemon, &SuperConductor::availableDevice, &desktopIntegration, &DesktopIntegration::availableDevice);
    QObject::connect(&desktopIntegration, &DesktopIntegration::acceptDevice, &daemon, &SuperConductor::acceptDevice);
//    QObject::connect(&desktopIntegration, &DesktopIntegration::sendToDevice, &daemon, &Daemon::sendToDevice);

//    QObject::connect(&daemon2, Daemon::availableDevice, &desktopIntegration, DesktopIntegration::availableDevice);
//    QObject::connect(&desktopIntegration, DesktopIntegration::acceptDevice, &daemon2, Daemon::acceptDevice);
//    QObject::connect(&desktopIntegration, DesktopIntegration::sendToDevice, &daemon2, Daemon::sendToDevice);

    desktopIntegration.show();
    daemon.start();
//    daemon2.start();

    return a.exec();
}
