#ifndef WINDOWSDESKTOPINTEGRATION_H
#define WINDOWSDESKTOPINTEGRATION_H

#include "desktopintegration.h"

#include <QMap>

class QMessageBox;
class QSystemTrayIcon;
class QMenu;
class QAction;

class QtDesktopIntegration : public DesktopIntegration
{
public:
  QtDesktopIntegration(const QSslCertificate &certificate, const QString &casPath, QObject *parent = 0);
  ~QtDesktopIntegration();

  // DesktopIntegration interface
public slots:
  void show();
  void availableDevice(const QSslCertificate &clientCert);

private slots:
  void requestPairing(QSslCertificate device);

private:
  const QSslCertificate m_certificate;
  const QString m_casPath;

  QSystemTrayIcon *m_tray;
  QMenu *m_availableDevicesMenu, *m_pairedDevicesMenu;
};

#endif // WINDOWSDESKTOPINTEGRATION_H
