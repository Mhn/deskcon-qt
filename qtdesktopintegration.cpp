#include "qtdesktopintegration.h"

#include <QDebug>
#include <QSslKey>
#include <QCryptographicHash>
#include <QMessageBox>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QMenu>
#include "pingplugin.h"
#include "pluginmanager.h"

QtDesktopIntegration::QtDesktopIntegration(const QSslCertificate &certificate, const QString &casPath, QObject *parent) :
  DesktopIntegration(parent),
  m_certificate(certificate),
  m_casPath(casPath)
{
}

QtDesktopIntegration::~QtDesktopIntegration()
{
}

void QtDesktopIntegration::show()
{
  if (QSystemTrayIcon::isSystemTrayAvailable())
  {
    m_tray = new QSystemTrayIcon(this);

    QMenu *trayMenu = new QMenu();

    m_availableDevicesMenu = trayMenu->addMenu("Available devices");
    m_pairedDevicesMenu = trayMenu->addMenu("Paired devices");

    m_tray->setContextMenu(trayMenu);

    m_tray->show();
  }
}

void QtDesktopIntegration::availableDevice(const QSslCertificate &clientCert)
{
  if (QSslCertificate::fromPath(m_casPath).contains(clientCert))
  {
    QAction *menuEntry = m_pairedDevicesMenu->addAction(clientCert.subjectInfo(QSslCertificate::CommonName).first());
    connect(menuEntry, &QAction::triggered, this, [=]() {
      if (Plugin *p = PluginManager::instance()->getPlugin("ping"))
      {
        PingPlugin* pingPlugin = qobject_cast<PingPlugin*>(p);
        pingPlugin->ping(clientCert, "ping");
      }
    });
  }
  else
  {
    QAction *menuEntry = m_availableDevicesMenu->addAction(clientCert.subjectInfo(QSslCertificate::CommonName).first());
    connect(menuEntry, &QAction::triggered, this, [=]() {
      requestPairing(clientCert);
    });
  }
}

void QtDesktopIntegration::requestPairing(QSslCertificate device)
{
  qDebug() << "Request pairing";
  QSslCertificate clientCert = device;

  QByteArray servercert_der = m_certificate.toDer();
  QByteArray serverhash_sha = QCryptographicHash::hash(servercert_der, QCryptographicHash::Sha256);
  qDebug() << "Server SHA256:" << serverhash_sha.toHex().toUpper();

  QByteArray clientcert_der = clientCert.toDer();
  QByteArray clienthash_sha = QCryptographicHash::hash(clientcert_der, QCryptographicHash::Sha256);
  qDebug() << "Client SHA256:" << clienthash_sha.toHex().toUpper();

  QMessageBox* msgBox = new QMessageBox();
  msgBox->setWindowTitle("Device pairing request");
  msgBox->setText(tr("Does the following hashes look identical to the ones displayed on the device?"));


  QString msg(tr("%1: %2\n%3: %4"));
  if (m_certificate.subjectInfo(QSslCertificate::CommonName).first().compare(clientCert.subjectInfo(QSslCertificate::CommonName).first()) > 0)
    msg = tr("%3: %4\n%1: %2");

  msg = msg
      .arg(clientCert.subjectInfo(QSslCertificate::CommonName).first())
      .arg(QString(clienthash_sha.toHex().toUpper()))
      .arg(m_certificate.subjectInfo(QSslCertificate::CommonName).first())
      .arg(QString(serverhash_sha.toHex().toUpper()));

  msgBox->setInformativeText(msg);
  msgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
  msgBox->setDefaultButton(QMessageBox::No);
  connect(msgBox, &QMessageBox::finished, this, [=](int result){
    if (result == QMessageBox::Yes)
    {
        qDebug() << "Accepted pairing";
        emit acceptDevice(clientCert);
    }
    else
        qDebug() << "Rejected pairing";
    msgBox->deleteLater();
  });
  msgBox->show();
}
