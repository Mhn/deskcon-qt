#ifndef PINGPLUGIN_H
#define PINGPLUGIN_H


#include "plugin.h"
#include "pluginmanager.h"

class PingPlugin : public Plugin
{
  Q_OBJECT
public:
  PingPlugin(QObject *parent = 0);
  ~PingPlugin();

public slots:
  void ping(QSslCertificate device, QString message = QString());

public slots:
  void receivedFromDevice(const QSslCertificate &clientCert, const QVariantMap &data, QSslSocket *socket = nullptr);
};

#endif // PINGPLUGIN_H
