#include "pingplugin.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>


static PluginFactoryTemplate<PingPlugin> typeFactory_;
static PluginFactoryRegister type("ping", &typeFactory_, PluginFactory::instance());


PingPlugin::PingPlugin(QObject *parent) :
  Plugin(parent)
{

}

PingPlugin::~PingPlugin()
{

}

void PingPlugin::ping(QSslCertificate device, QString message)
{
  QVariantMap data;
  data["msg"] = message;
  emit sendToDevice(device, "ping", data);
}

void PingPlugin::receivedFromDevice(const QSslCertificate &clientCert, const QVariantMap &data, QSslSocket *socket)
{
  if (socket)
    qWarning() << "Ping plugin got a connection on a sidechannel! It does not want that!";

  qDebug() << "Pingplugin got a message!" << data.value("msg").toString();
  if (data.value("msg").toString() == "ping")
  {
    qDebug() << "Responding with pong";
    QVariantMap resp;
    resp["msg"] = "pong";
    emit sendToDevice(clientCert, "ping", resp);
  }
}

