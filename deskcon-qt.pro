#-------------------------------------------------
#
# Project created by QtCreator 2015-01-14T22:02:50
#
#-------------------------------------------------

QT       += core network widgets

QT       -= gui

TARGET = deskcon-qt
CONFIG   += console
CONFIG   -= app_bundle
CONFIG += c++11

TEMPLATE = app


SOURCES += main.cpp \
    daemon.cpp \
    qsslserver.cpp \
    qtdesktopintegration.cpp \
    pingplugin.cpp \
    pluginmanager.cpp

HEADERS += \
    daemon.h \
    desktopintegration.h \
    qsslserver.h \
    qtdesktopintegration.h \
    plugin.h \
    pingplugin.h \
    pluginmanager.h

win32: LIBS += -llibqca

unix: CONFIG += link_pkgconfig
unix: PKGCONFIG += qca2-qt5

win32: LIBS += -L$$PWD/../build-superconductor-Desktop_Qt_5_4_1_MinGW_32bit-Debug/debug/ -lsuperconductor

INCLUDEPATH += $$PWD/../superconductor
DEPENDPATH += $$PWD/../superconductor

win32: PRE_TARGETDEPS += $$PWD/../build-superconductor-Desktop_Qt_5_4_1_MinGW_32bit-Debug/debug/libsuperconductor.a

#win32: LIBS += -llibqca.dll
