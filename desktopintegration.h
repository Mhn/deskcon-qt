#ifndef DESKTOPINTEGRATION_H
#define DESKTOPINTEGRATION_H

#include <QObject>
#include <QSslCertificate>

class QSslSocket;

class DesktopIntegration : public QObject
{
  Q_OBJECT
public:
  DesktopIntegration(QObject *parent = 0) : QObject(parent) {}
  virtual ~DesktopIntegration() {}

signals:
  void acceptDevice(const QSslCertificate &clientCert);
  void sendToDevice(const QSslCertificate &clientCert, const QByteArray &data);

public slots:
  virtual void show() = 0;
  virtual void availableDevice(const QSslCertificate &clientCert) = 0;
};

#endif // DESKTOPINTEGRATION_H
