#include "pluginmanager.h"

#include "plugin.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QDebug>

PluginFactory *PluginFactory::m_instance = nullptr;

PluginManager *PluginManager::m_instance = nullptr;

PluginManager::PluginManager(QObject *parent) : QObject(parent)
{
}

Plugin *PluginManager::getPlugin(QString pluginName)
{
  bool justCreated;
  Plugin *plugin = PluginFactory::instance()->create(pluginName, 0, &justCreated);
  if (justCreated)
    connect(plugin, &Plugin::sendToDevice, this, &PluginManager::sendToDevice_);
  return plugin;
}

void PluginManager::sendToDevice_(const QSslCertificate &clientCert, const QString &receiver, const QVariantMap &data)
{
  QJsonObject pluginData = QJsonObject::fromVariantMap(data);

  QJsonObject payLoad;
  payLoad["to"] = receiver;
  payLoad["data"] = pluginData;

  QJsonDocument doc(payLoad);

  emit sendToDevice(clientCert, doc.toJson());
}

void PluginManager::receivedFromDevice(const QSslCertificate &clientCert, const QByteArray &data, QSslSocket *socket)
{
  QJsonDocument doc = QJsonDocument::fromJson(data);

  if (!doc.isNull())
  {
    QJsonObject json = doc.object();

    if (json.contains("to"))
    {
      QString to = json.value("to").toString();

      QList<QString> types = PluginFactory::instance()->types();

      if (types.contains(to))
      {
        Plugin *plugin = getPlugin(to);

        plugin->receivedFromDevice(clientCert, json.value("data").toObject().toVariantMap(), socket);
      }
      else
        qDebug() << "No plugin for handling type" << to << data;
    }
    else
      qDebug() << "No target for messgae:" << data;
  }
  else
    qDebug() << "Invalid data received from device" << clientCert.subjectInfo(QSslCertificate::CommonName) << data;
}
