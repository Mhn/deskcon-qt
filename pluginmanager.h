#ifndef PLUGINMANAGER_H
#define PLUGINMANAGER_H

#include <QObject>
#include <QSslCertificate>
#include <QStringList>

class QSslSocket;
class Plugin;

////////////////////////////////////////////////////////////////////////////////
typedef Plugin FactoryType;

class PluginFactoryInterface
{
public:
  virtual FactoryType *create(QObject *parent) = 0;
};

template <typename T>
class PluginFactoryTemplate : public PluginFactoryInterface
{
  FactoryType *create(QObject *parent = 0)
  {
    return new T(parent);
  }
};

struct PluginFactoryData
{
  PluginFactoryInterface *factory;
  FactoryType *obj;
};

class PluginFactory
{
public:
  static PluginFactory *instance()
  {
    if (!m_instance)
      m_instance = new PluginFactory();
    return m_instance;
  }
  FactoryType *create(const QString &type, QObject *parent = 0, bool *justCreated = 0)
  {
    if (types_.contains(type) == false)
      return nullptr;
    else if (!types_.value(type).obj)
    {
      types_[type].obj = types_.value(type).factory->create(parent);
      if (justCreated)
        *justCreated = true;
    }
    else if (justCreated)
        *justCreated = false;

    return types_.value(type).obj;
  }

  void registerType(const QString &type, PluginFactoryInterface *factory)
  {
    if (types_.contains(type) == false)
      types_.insert(type, PluginFactoryData{factory, nullptr});
  }

  QStringList types()
  {
    return types_.keys();
  }

private:
  PluginFactory() {}
  static PluginFactory *m_instance;
  QHash<QString, PluginFactoryData> types_;
};

class PluginFactoryRegister
{
public:
  PluginFactoryRegister(const QString &type,
                        PluginFactoryInterface *factory,
                        PluginFactory *instance)
  {
    instance->registerType(type, factory);
  }
};
////////////////////////////////////////////////////////////////////////////////



class PluginManager : public QObject
{
  Q_OBJECT
public:
  static PluginManager *instance()
  {
    if (!m_instance)
      m_instance = new PluginManager();
    return m_instance;
  }
  Plugin* getPlugin(QString pluginName);

signals:
  void sendToDevice(const QSslCertificate &clientCert, const QByteArray &data);

public slots:
  void sendToDevice_(const QSslCertificate &clientCert, const QString &receiver, const QVariantMap &data);
  void receivedFromDevice(const QSslCertificate &clientCert, const QByteArray &data, QSslSocket *socket = nullptr);

private:
  PluginManager(QObject *parent = 0);
  static PluginManager *m_instance;
//  PluginFactory m_pluginFactory;
};

#endif // PLUGINMANAGER_H
